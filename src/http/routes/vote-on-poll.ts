import { z } from "zod";
import { prisma } from "../../lib/prisma";
import { FastifyInstance } from "fastify";
import { randomUUID } from "crypto";
import { redis } from "../../lib/redis";
import { votting } from "../../utils/voting-pub-sub";

export async function voteOnPoll(app: FastifyInstance) {
    
    app.post('/polls/:pollId/votes', async (request, response) => {
    
        const voteOnPollBody = z.object({
            pollOptionId: z.string().uuid(),
        });

        const voteOnPatams = z.object({
            pollId: z.string().uuid()
        });

        const { pollId } = voteOnPatams.parse(request.params);
        const { pollOptionId } = voteOnPollBody.parse(request.body);
        
        let { sessionId } = request.cookies;

        if (sessionId) {
            const userPreviousVoteOnPoll = await prisma.vote.findUnique({
                where: {
                    sessionId_pollId: {
                        sessionId,
                        pollId
                    }
                }
            });

            if (userPreviousVoteOnPoll && userPreviousVoteOnPoll.pollOptionId !== pollOptionId) {
                
                await prisma.vote.delete({
                    where: {
                        id: userPreviousVoteOnPoll.id
                    }
                });

                const votes = await redis.zincrby(pollId, -1, userPreviousVoteOnPoll.pollOptionId);

                votting.publish(pollId, {
                    pollOptionId: userPreviousVoteOnPoll.pollOptionId,
                    votes: Number(votes)
                });

            }
            else if (userPreviousVoteOnPoll) {
                return response.status(400).send({message: 'Você já votou anteriomente nessa opção.'});
            }
        }

        if (!sessionId) {

            const sessionId = randomUUID();
            
            response.setCookie("sessionId", sessionId, {
                path: "/",
                maxAge: 60*60*24*30, // 30 dias
                signed: true,
                httpOnly: true
            });
        }

        await prisma.vote.create({
            data: {
                sessionId,
                pollId,
                pollOptionId
            }
        });
        
        const votes = await redis.zincrby(pollId, 1, pollOptionId);

        votting.publish(pollId, {
            pollOptionId,
            votes: Number(votes)
        });

        return response.status(201).send({ sessionId })
    
    })

}