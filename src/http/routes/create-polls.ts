import { z } from "zod";
import { prisma } from "../../lib/prisma";
import { FastifyInstance } from "fastify";

export async function createPoll(app: FastifyInstance) {
    
    app.post('/polls', async (request, response) => {
    
        const createPollBody = z.object({
            title: z.string(),
            options: z.array(z.string()),
        });
    
        const { title, options } = createPollBody.parse(request.body);
        
        // modo raiz
        // const poll = await prisma.poll.create({
        //     data: {
        //         title
        //     }
        // })

        // await prisma.pollOption.createMany({
        //     data: options.map(option => {
        //         return { title: option, pollId: poll.id }
        //     })
        // });

        // padrão como se fosse o beginTransaction e criando relacionamentos
        const poll = await prisma.poll.create({
            data: {
                title,
                options: {
                    createMany: {
                        data: options.map(option => {
                            return { title: option }
                        })
                    }
                }
            }
        })

        await prisma.pollOption.createMany({
            data: options.map(option => {
                return { title: option, pollId: poll.id }
            })
        });
        
        return response.status(201).send({pollId: poll.id})
    
    })

}