# Inicio

```
npm init -y
```

## Instalar Typescript e Typescript do node
```
npm i typescript @types/node -D
```

## config Typescript
```
npx tsc --init
```

## converter ts para js e executar com node de forma automatizada
```
npm i tsx -D
```


## config package.json
```
"scripts": {
    "dev": "tsx watch src/http/server.ts"
  }
```

## instalar fastify
```
npm i fastify
```

## Rodar
```
npm run dev
```

## Prisma
```
npm i -D prisma
npx prisma init
```

## .env
```
DATABASE_URL="postgresql://docker:docker@localhost:5432/polls?schema=public"
```

## migrate banco
```
npx prisma migrate dev
npx prisma studio
```